<p align="center">
  <img alt="logo" src="https://codeberg.org/jkm/apb/raw/branch/master/images/logo.svg" />
</p>

# Alpinebook

A minimal microSD card rescue image for [Pinebook Pro](https://www.pine64.org/pinebook-pro/).

- 128 MiB size
- based on [Alpine Linux](https://alpinelinux.org)
- targeted at advanced users and developers
- useful when OS on your eMMC is unusable (happens often to me ;)

## Features

In this image you will get:

- window manager ([dwl](https://github.com/djpohly/dwl))
- terminal emulator ([foot](https://codeberg.org/dnkl/foot))
- terminal multiplexer ([tmux](https://github.com/tmux/tmux))
- text editor (vi)
- WiFi utility ([iw](https://wireless.wiki.kernel.org/en/users/Documentation/iw))
- WiFi daemon ([eiwd](https://codeberg.org/jkm/eiwd))
- process viewer ([htop](https://github.com/htop-dev/htop))
- package manager ([apk](https://wiki.alpinelinux.org/wiki/Package_management))

And yes, all of the above are running as `root`. If you're a Linux beginner, don't use this image.

## Installation

Installation is as easy as `dd` image to your microSD card:
```
$ dd if=apb-<version>.img of=/dev/mmcblk{id} bs=4M
```

## Additional steps

In order to make this OS fully functional, you'd probably like to take the following additional steps:

#### 1. Connect to WiFi network

Connecting to your WiFi network requires you to create `<your_wifi_ssid>.psk` file inside `/var/lib/eiwd` directory with the following structure:
```
[Security]
Passphrase=<your_wifi_passphrase>

[IP]
Address=192.168.1.2
Gateway=192.168.1.1
Netmask=255.255.255.0
```

You will, of course, need to adjust IP addresses to meet you requirements. After saving `.psk` file, `eiwd` daemon should automatically connect you to your WiFi network and set a local IP address.

#### 2. Resize rootfs partition and filesystem

- Firstly, you need to remount your rootfs to read-only mode:
```
$ mount -o remount,ro /dev/mmcblk{id}p2 /
```

If you'll see error saying `mount point is busy`, then do the following:
```
$ echo u > /proc/sysrq-trigger
```

- Secondly, you need to modify the partition table using `fdisk`:
```
$ fdisk /dev/mmcblk{id}
```

Delete partition `2` and then recreate it using the same start sector (69632). Use `w` to save changes. Don't worry if you'll see the following warning:
```
fdisk: WARNING: rereading partition table failed, kernel still uses old table: Resource busy
```
and simply `reboot` your machine.

- Lastly, resize ext4 filesystem using `resize2fs` utility:
```
$ resize2fs /dev/mmcblk{id}p2
```

## FAQ

- Does booting from eMMC work?
Not yet.
