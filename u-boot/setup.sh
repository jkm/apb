#!/bin/sh


CHROOT_DIR="tmp"

UBOOT_VERSION="v2022.01"
UBOOT_FILE="u-boot-${UBOOT_VERSION}.tar.gz"
UBOOT_URL="https://source.denx.de/u-boot/u-boot/-/archive/${UBOOT_VERSION}/${UBOOT_FILE}"
RK3399_FIRMWARE_FILE="rk3399_bl31_v1.35.elf"
RK3399_FIRMWARE_URL="https://raw.githubusercontent.com/rockchip-linux/rkbin/master/bin/rk33/${RK3399_FIRMWARE_FILE}"


# Get U-Boot archive
chroot $CHROOT_DIR wget -q $UBOOT_URL

# Unpack the U-Boot archive
chroot $CHROOT_DIR tar xf $UBOOT_FILE

# Make defconfig
chroot $CHROOT_DIR make -C u-boot-${UBOOT_VERSION} pinebook-pro-rk3399_defconfig

# Download RK3399 firmware file
chroot $CHROOT_DIR wget -q $RK3399_FIRMWARE_URL -O u-boot-${UBOOT_VERSION}/bl31.elf

# Compile U-Boot
chroot $CHROOT_DIR make -C u-boot-${UBOOT_VERSION}

# Copy artifacts
cp $CHROOT_DIR/u-boot-${UBOOT_VERSION}/u-boot.itb u-boot
cp $CHROOT_DIR/u-boot-${UBOOT_VERSION}/idbloader.img u-boot
