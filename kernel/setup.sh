#!/bin/sh


CHROOT_DIR="tmp"

KERNEL_VERSION="5.10.106"
KERNEL_FILE="linux-${KERNEL_VERSION}.tar.xz"
KERNEL_URL="https://cdn.kernel.org/pub/linux/kernel/v5.x/${KERNEL_FILE}"


# Get Linux kernel archive
chroot $CHROOT_DIR wget -q $KERNEL_URL

# Unpack the kernel archive
chroot $CHROOT_DIR tar xf $KERNEL_FILE

# Copy AP6256 firmware
mkdir -p $CHROOT_DIR/lib/firmware/brcm
cp -r kernel/firmware/brcm $CHROOT_DIR/lib/firmware

# Copy .config file
cp kernel/.config tmp/linux-${KERNEL_VERSION}

# Compile the kernel image and device tree
chroot $CHROOT_DIR make -C linux-${KERNEL_VERSION} ARCH=arm64 Image
chroot $CHROOT_DIR make -C linux-${KERNEL_VERSION} ARCH=arm64 dtbs

# Copy artifacts
cp $CHROOT_DIR/linux-${KERNEL_VERSION}/arch/arm64/boot/Image boot
cp $CHROOT_DIR/linux-${KERNEL_VERSION}/arch/arm64/boot/dts/rockchip/rk3399-pinebook-pro.dtb boot
