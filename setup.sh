#!/bin/sh


CHROOT_DIR="tmp"
MOUNT_DIR="/mnt"

ALPINEBOOK_VERSION="v22.03-rc1"
ALPINE_MAJOR_VERSION="3.15"
ALPINE_MINOR_VERSION="1"
ALPINE_ROOTFS_FILE="alpine-minirootfs-${ALPINE_MAJOR_VERSION}.${ALPINE_MINOR_VERSION}-aarch64.tar.gz"


# Detach all mounted images
losetup -D

# Remove old image
rm -f apb-${ALPINEBOOK_VERSION}.img

# Setup image file
dd if=/dev/zero of=apb-${ALPINEBOOK_VERSION}.img bs=512 count=262144 status=none

# Attach image to loop device
losetup -P /dev/loop0 apb-${ALPINEBOOK_VERSION}.img

# Partition disk image
sfdisk -q /dev/loop0 < disk/layout

# Make filesystems
mkfs.vfat /dev/loop0p1
mkfs.ext4 -q /dev/loop0p2

# Boot partition
mount /dev/loop0p1 $MOUNT_DIR
cp -r boot/* $MOUNT_DIR
find $MOUNT_DIR | xargs touch -h
umount /dev/loop0p1

# Mount rootfs volume
mount /dev/loop0p2 $MOUNT_DIR

# Unpack Alpine Linux rootfs
tar xf $ALPINE_ROOTFS_FILE -C $MOUNT_DIR

# Create directories
mkdir -p $MOUNT_DIR/root/.config/foot
mkdir $MOUNT_DIR/var/lib/eiwd
mkdir $MOUNT_DIR/usr/share/consolefonts

# Copy necessary files
cp etc/inittab $MOUNT_DIR/etc/inittab
cp etc/profile $MOUNT_DIR/etc/profile
cp etc/init.d/rcS $MOUNT_DIR/etc/init.d/rcS

# Copy binaries
cp $CHROOT_DIR/usr/bin/eiwd $MOUNT_DIR/usr/bin/eiwd
cp $CHROOT_DIR/usr/local/bin/dwl $MOUNT_DIR/usr/local/bin/dwl
cp $CHROOT_DIR/usr/local/bin/foot $MOUNT_DIR/usr/local/bin/foot

# Copy local libraries
cp $CHROOT_DIR/usr/local/lib/lib* $MOUNT_DIR/usr/local/lib

# Copy Mesa DRI libraries
cp -r $CHROOT_DIR/usr/local/lib/dri $MOUNT_DIR/usr/local/lib

# Copy .psk files
if [ -f /var/lib/eiwd/*.psk ]; then
  cp /var/lib/eiwd/*.psk $MOUNT_DIR/var/lib/eiwd
fi

# Copy foot themes
cp -r $CHROOT_DIR/usr/local/share/foot $MOUNT_DIR/usr/local/share

# Copy foot terminfo
cp -r $CHROOT_DIR/usr/local/share/terminfo $MOUNT_DIR/usr/local/share

# Copy foot.ini file
cp rootfs/foot.ini $MOUNT_DIR/root/.config/foot

# Set hostname
echo apb > $MOUNT_DIR/etc/hostname

# Set default DNS server
echo "nameserver 1.1.1.1" > $MOUNT_DIR/etc/resolv.conf

# Update packages
chroot $MOUNT_DIR apk -q update

# Install packages
chroot $MOUNT_DIR apk -q add \
			e2fsprogs \
			e2fsprogs-extra \
			eudev \
			expat \
			fontconfig \
			htop \
			iw \
			libdrm \
			libffi \
			libgcc \
			libinput-libs \
			libseat \
			libstdc++ \
			libxkbcommon \
			libxml2 \
			ncurses \
			pixman \
			seatd \
			tmux \
			ttf-inconsolata \
			zstd-libs

# Upgrade packages
chroot $MOUNT_DIR apk -q upgrade

# Touch all files
find $MOUNT_DIR | xargs touch -h

# Umount rootfs volume
umount /dev/loop0p2

# Flash bootloader
dd if=boot/u-boot.itb of=/dev/loop0 seek=16384 status=none
dd if=boot/idbloader.img of=/dev/loop0 seek=64 status=none

# Detach image from loop device
losetup -d /dev/loop0

# Copy image to given block device
if [ ! -z $1 ]; then
  dd if=apb-${ALPINEBOOK_VERSION}.img of=$1 bs=4M status=none
fi
