#!/bin/sh


CHROOT_DIR="tmp"

DWL_REPO="https://github.com/djpohly/dwl.git"
EIWD_REPO="https://codeberg.org/jkm/eiwd.git"

WAYLAND_VERSION="1.20.0"
WAYLAND_FILE="wayland-${WAYLAND_VERSION}.tar.xz"
WAYLAND_URL="https://wayland.freedesktop.org/releases/${WAYLAND_FILE}"

WAYLAND_PROTOCOLS_VERSION="1.25"
WAYLAND_PROTOCOLS_FILE="wayland-protocols-${WAYLAND_PROTOCOLS_VERSION}.tar.xz"
WAYLAND_PROTOCOLS_URL="https://wayland.freedesktop.org/releases/${WAYLAND_PROTOCOLS_FILE}"

WLROOTS_VERSION="0.15.1"
WLROOTS_FILE="wlroots-${WLROOTS_VERSION}.tar.gz"
WLROOTS_URL="https://gitlab.freedesktop.org/wlroots/wlroots/-/releases/${WLROOTS_VERSION}/downloads/${WLROOTS_FILE}"

MESA_VERSION="22.0.0"
MESA_FILE="mesa-${MESA_VERSION}.tar.xz"
MESA_URL="https://archive.mesa3d.org/${MESA_FILE}"

FOOT_VERSION="1.11.0"
FOOT_FILE="${FOOT_VERSION}.tar.gz"
FOOT_URL="https://codeberg.org/dnkl/foot/archive/${FOOT_FILE}"


# Install necessary dependencies
chroot $CHROOT_DIR apk add -q \
			eudev-dev \
			expat-dev \
			fontconfig-dev \
			ncurses \
			libdrm-dev \
			libevdev-dev \
			libffi-dev \
			libinput-dev \
			libseat-dev \
			libxkbcommon-dev \
			llvm12-dev \
			pixman-dev \
			zlib-dev \
			zstd-dev

# Install pip
chroot $CHROOT_DIR wget -q https://bootstrap.pypa.io/get-pip.py
chroot $CHROOT_DIR python3 get-pip.py

# Install Python tools
chroot $CHROOT_DIR pip install -q Mako meson

# Clone eiwd repository
chroot $CHROOT_DIR git clone $EIWD_REPO

# Compile eiwd binary
chroot $CHROOT_DIR make -C eiwd
chroot $CHROOT_DIR make -C eiwd install

# Get Wayland archive
chroot $CHROOT_DIR wget -q $WAYLAND_URL

# Unpack Wayland archive
chroot $CHROOT_DIR tar xf $WAYLAND_FILE

# Compile Wayland
chroot $CHROOT_DIR meson wayland-$WAYLAND_VERSION build -Dtests=false -Ddocumentation=false -Ddtd_validation=false
chroot $CHROOT_DIR ninja -C build
chroot $CHROOT_DIR ninja -C build install
chroot $CHROOT_DIR rm -rf build

# Get Wayland Protocols archive
chroot $CHROOT_DIR wget -q $WAYLAND_PROTOCOLS_URL

# Unpack Wayland Protocols archive
chroot $CHROOT_DIR tar xf $WAYLAND_PROTOCOLS_FILE

# Install Wayland Protocols
chroot $CHROOT_DIR meson wayland-protocols-$WAYLAND_PROTOCOLS_VERSION build -Dtests=false
chroot $CHROOT_DIR ninja -C build install
chroot $CHROOT_DIR rm -rf build

# Get Mesa archive
chroot $CHROOT_DIR wget -q $MESA_URL

# Unpack Mesa archive
chroot $CHROOT_DIR tar xf $MESA_FILE

# Compile Mesa
chroot $CHROOT_DIR meson mesa-$MESA_VERSION build \
			-Ddri-drivers= \
			-Dvulkan-drivers= \
			-Dgallium-drivers=kmsro,panfrost \
			-Dplatforms=wayland \
			-Dgbm=enabled \
			-Dglx=disabled \
			-Dgles1=disabled \
			-Dgles2=enabled \
			-Dllvm=disabled \
			-Dbuildtype=plain \
			-Db_ndebug=true

chroot $CHROOT_DIR ninja -C build
chroot $CHROOT_DIR ninja -C build install
chroot $CHROOT_DIR rm -rf build

# Get wlroots archive
chroot $CHROOT_DIR wget -q $WLROOTS_URL

# Unpack wlroots archive
chroot $CHROOT_DIR tar xf $WLROOTS_FILE

# Compile wlroots
chroot $CHROOT_DIR meson wlroots-$WLROOTS_VERSION build -Dxwayland=disabled -Dexamples=false -Dbackends=drm,libinput -Drenderers=gles2
chroot $CHROOT_DIR ninja -C build
chroot $CHROOT_DIR ninja -C build install
chroot $CHROOT_DIR rm -rf build

# Clone dwl repository
chroot $CHROOT_DIR git clone $DWL_REPO

# Patch dwl source tree
chroot $CHROOT_DIR patch -s -p0 -d dwl < rootfs/dwl.diff

# Compile dwl
chroot $CHROOT_DIR make -C dwl
chroot $CHROOT_DIR make -C dwl install

# Get foot archive
chroot $CHROOT_DIR wget -q $FOOT_URL

# Unpack foot archive
chroot $CHROOT_DIR tar xf $FOOT_FILE

# Compile foot
chroot $CHROOT_DIR meson foot build --buildtype=release -Ddocs=disabled
chroot $CHROOT_DIR ninja -C build
chroot $CHROOT_DIR ninja -C build install
chroot $CHROOT_DIR rm -rf build
