#!/bin/sh


CHROOT_DIR="tmp"

ALPINE_ARCH="aarch64"
ALPINE_MAJOR_VERSION="3.15"
ALPINE_MINOR_VERSION="1"
ALPINE_ROOTFS_FILE="alpine-minirootfs-${ALPINE_MAJOR_VERSION}.${ALPINE_MINOR_VERSION}-${ALPINE_ARCH}.tar.gz"
ALPINE_ROOTFS_URL="https://dl-cdn.alpinelinux.org/alpine/v${ALPINE_MAJOR_VERSION}/releases/${ALPINE_ARCH}/${ALPINE_ROOTFS_FILE}"


# Download Alpine Linux rootfs
if [ ! -f ${ALPINE_ROOTFS_FILE} ]; then
  wget -q $ALPINE_ROOTFS_URL
  wget -q $ALPINE_ROOTFS_URL.sha256
fi

# Verify checksum
sha256sum -s -c ${ALPINE_ROOTFS_FILE}.sha256
if [ $? -ne 0 ]; then
  echo "Malformed file"
  exit 1
fi

# Create chroot
mkdir $CHROOT_DIR
tar xf $ALPINE_ROOTFS_FILE -C $CHROOT_DIR
echo "nameserver 1.1.1.1" > ${CHROOT_DIR}/etc/resolv.conf
chroot $CHROOT_DIR hostname apb
chroot $CHROOT_DIR apk -q update
chroot $CHROOT_DIR apk -q add bison dtc flex gcc g++ git linux-headers make musl-dev ninja openssl-dev patch perl python3 wireless-regdb

# Mount pseudo filesystems
mount -t proc /proc $CHROOT_DIR/proc
mount --rbind /sys $CHROOT_DIR/sys
mount --rbind /dev $CHROOT_DIR/dev
